import os
import sys
import copy
import time
import random
import tkinter as tk
from tkinter import messagebox, filedialog
from PIL import Image, ImageTk, ImageDraw

from space import Space, Grower, Neighborhood

ITER_LIMIT = 5000

NEIGHBORHOOD = {
    'moore': Neighborhood.MOORE,
    'von-neumann': Neighborhood.VON_NEUMANN
}

class SpaceImage:
    def __init__(self, image_tk=None, image=None, imagedraw=None):
        self.img_tk = image_tk
        self.img = image
        self.draw = imagedraw
        self.space = None

active_grains = []

def update_pre_space(pre_space):
    """Generate space basing on active grains."""
    for x in range(0, pre_space.size[0]):
        for y in range(0, pre_space.size[1]):
            if pre_space.at(x, y).value not in active_grains:
                pre_space.at(x, y).value = 0
                pre_space.at(x, y).color = (255, 255, 255)

def on_grain_click(event):
    """Handle grain clicked event."""
    x = event.x
    y = event.y
    print('[DEBUG] Clicked ' + str(x) + ' ' + str(y))

    # Normalize coordinates
    if x > space_img.space.size[0]:
        x = space_img.space.size[0] - 1
    if y > space_img.space.size[1]:
        y = space_img.space.size[1] - 1

    grain_no = space_img.space.at(x, y).value
    if grain_no not in active_grains and grain_no is not None:
        active_grains.append(grain_no)
    else:
        active_grains.remove(grain_no)

    print('[DEBUG] Active grains: ' + str(active_grains))

def bind_cv_event(obj):
    """Binds events to canvas."""
    cv.tag_bind(obj, '<ButtonPress-1>', on_grain_click)

def export():
    filename = filedialog.asksaveasfilename(
        initialdir=".",
        title="Export to file",
        filetypes=(("CSV, Comma-separated Value","*.csv"),
                   ("BMP, Bitmap", "*.bmp"),))

    if not filename:
        return

    if filename[-4:] == '.csv':
        print('Colors: ' + str(space_img.space.colors))
        colors_str = ''
        for color in space_img.space.colors:
            colors_str += '{} '.format(str(color).replace(' ', ''))
        colors_str.strip()

        try:
            with open(filename, mode='w') as new_file:
                new_file.write('Colors\n')
                new_file.write(colors_str)
                new_file.write('\nSpace\n')
                new_file.write(str(space_img.space).replace(' ', ','))
        except IOError:
            print('[ERROR] Unable to save file.', file=sys.stderr)
        else:
            print('[SUCCESS] File saved: {}'.format(filename))
    elif filename[-4:] == '.bmp':
        try:
            space_img.img.save(filename, format='bmp')
        except ValueError:
            print('[ERROR] Unknown format specified. File not saved.',
                  file=sys.stderr)
        except IOError:
            print('[ERROR] Unable to save image file.', file=sys.stderr)
        else:
            print('[SUCCESS] File saved: {}'.format(filename))

def file_import():
    def draw_img(coords):
        for point in coords:
            x = point[0]
            y = point[1]
            cell_val = space_img.space.get_cells()[x][y].value
            if cell_val.isnumeric():
                # color = space_img.space.colors[int(cell_val)-1]
                # draw.point([x, y], fill=color)
                draw.point([x, y], fill=space_img.space.get_cells()[x][y].color)
            else:
                color = (0, 0, 0)
                draw.point([x, y], fill=color)

        space_img.img_tk = ImageTk.PhotoImage(space_img.img)
        cv_img = cv.create_image(0, 0, image=space_img.img_tk, anchor='nw')
        bind_cv_event(cv_img)
        window.update()

    filename = filedialog.askopenfilename(
        initialdir=".",
        title="Import from file",
        filetypes=(("CSV, Comma-separated Value", "*.csv"),
                   ("BMP, Bitmap", "*.bmp")))

    if not filename:
        return

    if filename[-4:] == '.csv':
        with open(filename, mode='r') as imported:
            space_img.space = Space((0, 0)).from_file(imported)
    elif filename[-4:] == '.bmp':
        with Image.open(filename) as imported:
            space_img.space = Space((0, 0)).from_bmp(imported)
    else:
        raise ValueError('[ERROR] Unsupported file extension.')

    img_space = Image.new('RGB', space_img.space.size, (255, 255, 255))
    space_img.img = img_space
    draw = ImageDraw.Draw(space_img.img)
    space_img.draw = draw

    space_img.img_tk = ImageTk.PhotoImage(space_img.img)
    cv.create_image(0, 0, image=space_img.img_tk, anchor='nw')
    window.update()

    neighborhood = translate(var_neighborhood.get())
    grower = Grower(space_img.space, NEIGHBORHOOD.get(neighborhood))
    colors = generate_colors(space_img.space.grains)

    print("[DEBUG] Imported params")
    print('[DEBUG] size: {}'.format(space_img.space.size))
    print('[DEBUG] grains: {}'.format(space_img.space.grains))
    print('[DEBUG] colors: {}'.format(space_img.space.colors))

    draw_img(grower.get_occupied())


def generate_colors(number):
    colors = []
    r = int(random.random() * 256)
    time.sleep(0.3)
    g = int(random.random() * 256)
    time.sleep(0.3)
    b = int(random.random() * 256)
    step = 256 / number
    for _ in range(number):
        r = int(r + step) % 256
        g = int(g + step) % 256
        b = int(b + step) % 256
        colors.append((r, g, b))

    return colors


def reset():
    space_img.img_tk = None
    space_img.img = None
    space_img.draw = None
    space_img.space = None
    active_grains.clear()


def clear_space():
    for x in range(space_img.space.size[0]):
        for y in range(space_img.space.size[1]):
            if space_img.img.getpixel((x, y)) != (0, 0, 0) or space_img.space.at(x, y) == 'x':
                space_img.draw.point([x, y], fill=(255, 255, 255))

    space_img.img_tk = ImageTk.PhotoImage(space_img.img)
    cv_img = cv.create_image(0, 0, image=space_img.img_tk, anchor='nw')
    bind_cv_event(cv_img)
    window.update()


def translate(name):
    translated = ''
    if name == 'Moore':
        translated = 'moore'
    elif name == 'Von Neumann':
        translated = 'von-neumann'
    elif name == 'Elementary':
        translated = 'elementary'
    elif name == 'None':
        translated = 'none'
    elif name == 'Random':
        translated = 'random'
    elif name == 'Random grain boundaries':
        translated = 'random-grain-bound'

    return translated

def run():
    def draw_grains(coords):
        for point in coords:
            x = point[0]
            y = point[1]
            cell_val = space.get_cells()[x][y].value
            cell_col = space.get_cells()[x][y].color
            if str(cell_val).isnumeric() and str(cell_val) != 'x':
                if int(cell_val) > 0:
                    # color = colors[int(cell_val)-1]
                    # draw.point([x, y], fill=color)
                    draw.point([x, y], fill=cell_col)

        space_img.img_tk = ImageTk.PhotoImage(space_img.img)
        cv_img = cv.create_image(0, 0, image=space_img.img_tk, anchor='nw')
        bind_cv_event(cv_img)
        window.update()

    def draw_precipitates(pcoords):
        for point in pcoords:
            space.get_cells()[point[0]][point[1]].value = 'x'
        draw.point(pcoords, fill=(0, 0, 0))

        space_img.img_tk = ImageTk.PhotoImage(space_img.img)
        cv_img = cv.create_image(0, 0, image=space_img.img_tk, anchor='nw')
        bind_cv_event(cv_img)
        window.update()

    prec_num = 0
    if text_prec_num.get():
        prec_num = int(text_prec_num.get())

    prec_rad = 1
    if text_prec_rad.get():
        prec_rad = int(text_prec_rad.get())

    gb_size = 0
    if text_gb_size.get():
        gb_size = int(text_gb_size.get())

    try:
        width = int(text_width.get())
        height = int(text_height.get())
        grains = int(text_grains.get())
    except ValueError:
        messagebox.showwarning('Warning', 'No input parameters provided.')
        return

    img_space = Image.new('RGB', (width, height), (255, 255, 255))
    space_img.img = img_space
    draw = ImageDraw.Draw(space_img.img)
    space_img.draw = draw

    neighborhood = translate(var_neighborhood.get())
    prec_pos = translate(var_precipitates_pos.get())
    colors = generate_colors(grains)
    space = Space((width, height), grains, colors)
    if space_img.space is not None and active_grains:
        update_pre_space(space_img.space)
        space = space_img.space
    space_img.space = space
    space_img.space.colors = colors
    grower = Grower(space_img.space, NEIGHBORHOOD.get(neighborhood))

    space_img.img_tk = ImageTk.PhotoImage(space_img.img)
    cv.create_image(0, 0, image=space_img.img_tk, anchor='nw')
    window.update()

    draw_grains(grower.get_occupied())

    print('\n=== GROWTH STARTED ===\n')

    iters = 0
    growth_time = time.time()
    while not space.filled() and iters < ITER_LIMIT:
        coords = grower.grow()
        iters += 1

        # Draw points
        draw_grains(coords)
        print('.', end='')

    if prec_pos == 'random-grain-bound':
        gb_coords = grower.get_grain_boundaries()

    if gb_size > 0:
        gb_coords = grower.get_grain_boundaries()
        draw.point(gb_coords, fill=(0, 0, 0))

    if prec_num:
        prec_coords = []
        if prec_pos == 'random':
            for _ in range(0, prec_num):
                rand_x = int(random.random() * width)
                rand_y = int(random.random() * height)
                prec_coords.append((rand_x, rand_y))
        elif prec_pos == 'random-grain-bound':
            for _ in range(0, prec_num):
                rand_coord = gb_coords[int(random.random() * len(gb_coords))]
                prec_coords.append(rand_coord)
        draw_precipitates(prec_coords)

        print('[DEBUG] prec rad: {}'.format(prec_rad))
        for r in range(1, prec_rad + 1):
            new_pcoords = grower.grow_precipitates(r, prec_coords)

            # Draw points of precipitates
            draw_precipitates(new_pcoords)

            time.sleep(0.3)
            print(',', end='')

    space_img.space = space

    growth_time = time.time() - growth_time

    print('\n=== GROWTH FINISHED ===\n')
    print('Iterations of growth: ' + str(iters))
    print('Time of growth: ' + str(growth_time) + ' s')

window = tk.Tk()

window.title("Modeling App")
window.geometry('800x900')

label_width = tk.Label(window, text="Space width")
label_height = tk.Label(window, text="Space height")
label_grains = tk.Label(window, text="Number of grains")
label_neighborhood = tk.Label(window, text="Neighborhood")
label_precipitates_pos = tk.Label(window, text="Precipitates position")
label_precipitates_num = tk.Label(window, text="Precipitates number")
label_precipitates_rad = tk.Label(window, text="Precipitate radius")
label_precipitates_time = tk.Label(window, text="Precipitates growth time")
label_gb_size = tk.Label(window, text="GB size")

label_width.place(x=15, y=15)
label_height.place(x=15, y=50)
label_grains.place(x=15, y=85)
label_neighborhood.place(x=15, y=120)
label_precipitates_pos.place(x=15, y=155)
label_precipitates_num.place(x=15, y=190)
label_precipitates_rad.place(x=15, y=225)
label_precipitates_time.place(x=15, y=260)
label_gb_size.place(x=15, y=295)

text_width = tk.Entry(window, width=15)
text_height = tk.Entry(window, width=15)
text_grains = tk.Entry(window, width=15)
text_prec_num = tk.Entry(window, width=15)
text_prec_rad = tk.Entry(window, width=15)
text_gb_size = tk.Entry(window, width=15)

text_width.place(x=150, y=50)
text_height.place(x=150, y=15)
text_grains.place(x=150, y=85)
text_prec_num.place(x=150, y=190)
text_prec_rad.place(x=150, y=225)
text_gb_size.place(x=150, y=295)

var_neighborhood = tk.StringVar(window)
var_neighborhood.set('Choose neighborhood')

choice_neighborhood = tk.OptionMenu(window, var_neighborhood, "Moore",
                                    "Von Neumann")

choice_neighborhood.place(x=150, y=120)

var_precipitates_pos = tk.StringVar(window)
var_precipitates_pos.set('Choose precipitates position')

choice_precipitates_pos = tk.OptionMenu(window, var_precipitates_pos, "None",
                                        "Random", "Random grain boundaries")

choice_precipitates_pos.place(x=150, y=155)

var_precipitates_time = tk.StringVar(window)
var_precipitates_time.set('Choose precipitates growth time')

choice_precipitates_time = tk.OptionMenu(window, var_precipitates_time, "End",
                                         "Beginning")

choice_precipitates_time.place(x=180, y=260)

cv = tk.Canvas(window, width=800, height=600)
cv.pack(side='bottom', fill='both', expand='yes')

cv.place(x=15, y=365)

btn_submit = tk.Button(window, text="Submit", command=run)
btn_export = tk.Button(window, text="Export", command=export)
btn_import = tk.Button(window, text="Import", command=file_import)
btn_reset = tk.Button(window, text="Reset", command=reset)
btn_clear = tk.Button(window, text="Clear space", command=clear_space)

btn_submit.place(x=15, y=330)
btn_export.place(x=100, y=330)
btn_import.place(x=180, y=330)
btn_reset.place(x=260, y=330)
btn_clear.place(x=340, y=330)

space_img = SpaceImage()

window.mainloop()
