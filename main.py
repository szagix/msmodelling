import os
import argparse

from space import Space, Grower, Neighborhood

ITER_LIMIT = 2000

HELP_SIZE = "Number of rows and columns in space separate with space."
HELP_GRAINS = "Number of grains."

parser = argparse.ArgumentParser()

parser.add_argument('--size', '-s', nargs=2, type=int, required=True,
                    help=HELP_SIZE)
parser.add_argument('--grains', '-g', nargs='?', type=int, default=0,
                    help=HELP_GRAINS)
parser.add_argument('--neighborhood', '-n', nargs='?', type=str,
                    default='moore', help=HELP_GRAINS)

args = parser.parse_args()

NEIGHBORHOOD = {
    'moore': Neighborhood.MOORE,
    'von-neumann': Neighborhood.VON_NEUMANN,
    'elementary': Neighborhood.ELEMENTARY
}

if args.neighborhood not in NEIGHBORHOOD.keys():
    raise Exception("[ERROR] Unknown neighborhood passed.")

space = Space((args.size[0], args.size[1]), args.grains)

print("=== Initial Space Status ===")
print(space)

grower = Grower(space, NEIGHBORHOOD.get(args.neighborhood))

print("=== Space Status Growth Steps ===")

iters = 0
while not space.filled() and iters < ITER_LIMIT:
    grower.grow()
    print(space)
    iters += 1

print("=== Final Space Status ===")
print(space)
