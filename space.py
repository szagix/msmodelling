import re
import enum
import random

SPACES = ['Space1D', 'Space2D', 'Space3D']


class Neighborhood(enum.Enum):
    """Types of neighborhood determination.

    There are two types of neighborhood:
        Moore (2D)
             _____
            |_|_|_|
            |_|x|_|
            |_|_|_|
        Von Neumann (2D)
               _
             _|_|_
            |_|x|_|
              |_|
    """
    MOORE = 1
    VON_NEUMANN = 2


class Cell:
    """Single entity that together with other cells creates a space.

    Depending on the cell value one can determine if the cell belongs to
    specific grain or not.
    """
    def __init__(self, value=0, color=None):
        """Initialize Cell object.

        Args:
            value (int): Grain number or 0 if cell does not belong to any grain.

        Attributes:
            value (int): Grain number or 0 if cell does not belong to any grain.
        """
        self.value = value
        self.color = color

    def __str__(self):
        """Returns string representation of Cell object."""
        return str(self.value)

    def occupied(self):
        if self.value:
            return True
        return False

    def precipitate(self):
        if isinstance(self.value, int):
            return False
        return True


class Grower:
    """Grain(s) growth responsible.
    """
    def __init__(self, space, neighborhood):
        self.x_size = space.size[0]
        self.y_size = space.size[1]
        self.s = space.get_cells()
        self.neighborhood = neighborhood

    def _grow_to(self, cell, num, color):
        cell.value = num if cell.value == 0 else cell.value
        cell.color = color if cell.color == (255, 255, 255) else cell.color

    def grow(self):
        """Spreads grains into all possible non-occupied directions in Space.

        Main method responsible for grain growth.

        Returns:
            new_coordinates (list of tuples): Coordinates newly occupied by
                grains.
        """
        coords = self.get_occupied()
        prev_coords = coords.copy()

        for coord in coords:
            x = coord[0]
            y = coord[1]
            num = self.s[x][y].value
            color = self.s[x][y].color
            if self.neighborhood == Neighborhood.MOORE:
                if y-1 >= 0:
                    self._grow_to(self.s[x][y-1], num, color)
                    if x-1 >= 0:
                        self._grow_to(self.s[x-1][y-1], num, color)
                    if x+1 < len(self.s):
                        self._grow_to(self.s[x+1][y-1], num, color)
                if x-1 >= 0:
                    self._grow_to(self.s[x-1][y], num, color)
                if x+1 < len(self.s):
                    self._grow_to(self.s[x+1][y], num, color)
                if y+1 < len(self.s[0]):
                    self._grow_to(self.s[x][y+1], num, color)
                    if x-1 >= 0:
                        self._grow_to(self.s[x-1][y+1], num, color)
                    if x+1 < len(self.s):
                        self._grow_to(self.s[x+1][y+1], num, color)
            elif self.neighborhood == Neighborhood.VON_NEUMANN:
                if y-1 >= 0:
                    self._grow_to(self.s[x][y-1], num, color)
                if x-1 >= 0:
                    self._grow_to(self.s[x-1][y], num, color)
                if x+1 < len(self.s):
                    self._grow_to(self.s[x+1][y], num, color)
                if y+1 < len(self.s[0]):
                    self._grow_to(self.s[x][y+1], num, color)
            else:
                raise Exception("[ERROR] Unknown neighborhood"
                                " specified.")

        # Get new state of grains.
        coords = self.get_occupied()

        return list(set(coords) - set(prev_coords))

    def grow_precipitates(self, radius, original_coords):
        """Grow precipitates up to specified radius."""
        curr_coords = self.get_precipitates()
        prev_coords = curr_coords.copy()

        for coord in original_coords:
            x = coord[0]
            y = coord[1]
            char = self.s[x][y].value
            color = self.s[x][y].color

            x_beg = x - radius
            y_beg = y - radius
            x_end = x + radius
            y_end = y + radius

            if x_beg < 0:
                x_beg = 0
            if y_beg < 0:
                y_beg = 0

            if x_end >= self.x_size:
                x_end = self.x_size - 1
            if y_end >= self.y_size:
                y_end = self.y_size - 1

            for xx in range(x_beg, x_end + 1):
                for yy in range(y_beg, y_end + 1):
                    if ((xx-x)*(xx-x) + (yy-y)*(yy-y) <= radius*radius and
                            self.s[xx][yy] != char):
                        self.s[xx][yy].value = char
                        self.s[xx][yy].color = color


        curr_coords = self.get_precipitates()

        return list(set(curr_coords) - set(prev_coords))

    def get_grain_boundaries(self):
        """Returns list of coordinates of grain boundaries."""
        coords = []

        for x in range(0, self.x_size):
            for y in range(0, self.y_size):
                val = self.s[x][y].value
                is_boundary = False
                if y-1 >= 0:
                    if self.s[x][y-1].value != val:
                        is_boundary = True
                if x-1 >= 0:
                    if self.s[x-1][y].value != val:
                        is_boundary = True
                if y+1 < self.y_size:
                    if self.s[x][y+1].value != val:
                        is_boundary = True
                if x+1 < self.x_size:
                    if self.s[x+1][y].value != val:
                        is_boundary = True
                if is_boundary:
                    coords.append((x, y))

        return coords

    def get_occupied(self):
        """Returns list of occupied Cells' coordinates.

        Helper method that creates a list containing Cells' coordinates that
        belong to grains.
        """
        coords = []

        # Get current state of grains.
        for x in range(0, self.x_size):
            for y in range(0, self.y_size):
                if self.s[x][y].occupied():
                    coords.append((x, y))

        return coords

    def get_precipitates(self):
        """Returns list of Cells' coordinates that belong to precipitate."""
        coords = []

        for x in range(0, self.x_size):
            for y in range(0, self.y_size):
                if self.s[x][y].precipitate():
                    coords.append((x, y))

        return coords


class Space:
    """Representation of 1D, 2D or 3D space filled with cells.

    Each cell can belong to specific grain (cell value != 0)
    or not (cell value == 0).
    """
    def __init__(self, size=(0,), grains=0, colors=[]):
        """Initialize Space object.

        Args:
            size (:obj:`tuple` of int): Tuple of integers specifying size of
                space.
            grains (int): Number of grains.

        Attributes:
            size (:obj:`tuple` of int): Tuple of integers specifying size of
                space.
            grains (int): Number of grains.
            __space (:obj:`Space1D` or :obj:`Space2D` or :obj:`Space3D`): Space
                object depending on dimension of the space.
        """
        self.size = size
        self.grains = grains
        self.colors = colors
        self.__space = Space2D(size, colors)

        if grains > 0:
            self.__space.init_grains(self.grains)

    def at(self, x=None, y=None, z=None):
        """Fetch value at given position in space."""
        cell = None
        if x is not None and y is not None and z is not None:
            cell = self.get_cells()[x][y][z]
        elif x is not None and y is not None and z is None:
            cell = self.get_cells()[x][y]
        elif x is not None and y is None and z is None:
            cell = self.get_cells()[x]

        return cell

    def filled(self):
        """Check if there are cells not filled with grain within space."""
        return self.__space.filled()

    def from_bmp(self, image):
        """Initialize space from bmp file."""
        self.colors = [elem[1] for elem in image.getcolors()]
        if (0, 0, 0) in self.colors:
            self.colors.remove((0, 0, 0))
        if (255, 255, 255) in self.colors:
            self.colors.remove((255, 255, 255))
        self.grains = len(self.colors)
        self.size = image.size

        matrix = []
        for x in range(image.width):
            matrix.append([])
            for y in range(image.height):
                px_val = image.getpixel((x, y))
                if px_val == (0, 0, 0):
                    matrix[x].append(Cell('x', (0, 0, 0)))
                elif px_val == (255, 255, 255):
                    matrix[x].append('0', (255, 255, 255))
                else:
                    matrix[x].append(Cell(str(self.colors.index(px_val)),
                                          px_val))

        self.__space = Space2D(self.size, self.colors)
        self.__space.cells = matrix

        return self

    def from_file(self, file_obj):
        """Initialize space from file."""
        i = 0
        j = 0
        matrix = []
        for line in file_obj.readlines():
            if re.search(r'Colors', line) or re.search(r'Space', line):
                continue
            if re.search(r' ', line):
                self.colors = []
                for col in line.split(' '):
                    if not col.strip():
                        continue
                    col = col.strip('()')
                    parts = col.split(',')
                    rgb = (int(parts[0]), int(parts[1]), int(parts[2]))
                    self.colors.append(rgb)
                continue

            matrix.append([])
            j = 0
            for c in line.split(','):
                j += 1
                if c != 'x' and c.isnumeric():
                    matrix[i].append(Cell(c, self.colors[int(c)-1]))
                    if int(c) > self.grains:
                        self.grains = int(c)
                else:
                    matrix[i].append(Cell(c, (0, 0, 0)))
            i += 1

        self.size = (i-1, j-2)
        self.__space = Space2D(self.size, self.colors)
        self.__space.cells = matrix

        return self

    def get_cells(self):
        """Returns Space's matrix."""
        return self.__space.cells

    def init_grains(self, number):
        """Place specified number of grains on space randomly.

        Args:
            number (int): Number of grains to put on space.
        """
        self.__space.init_grains(number)

    def __str__(self):
        return self.__space.__str__()


class Space1D:
    """Representation of 1D space filled with cells."""
    def __init__(self, size=(0,)):
        """Initialize 1D Space object.

        Args:
            size (:obj:`tuple` of int): Tuple of integers specifying size of
                space.
        """
        self.size = size
        self.cells = [Cell() for x in range(0, size[0])]

    def __str__(self):
        return ' '.join(self.cells)

class Space2D:
    """Representation of 2D space filled with cells."""
    def __init__(self, size=(0, 0,), colors=[]):
        """Initialize 2D Space object.

        Args:
            size (:obj:`tuple` of int): Tuple of integers specifying size of
                space.

        Attributes:
            size (:obj:`tuple` of int): Tuple of integers specifying size of
                space.
        """
        self.size = size
        self.cells = [[Cell(0, (255, 255, 255)) for y in range(0, size[1])] \
                      for x in range(0, size[0])]
        self.colors = colors

    def filled(self):
        """Check if there are cells not filled with grain within space."""
        for col in self.cells:
            for cell in col:
                if cell.value == 0:
                    return False
        return True

    def __get_cells_coords(self):
        coords = []
        for x in range(0, len(self.cells)):
            for y in range(0, len(self.cells[x])):
                coords.append([x, y])

        return coords

    def init_grains(self, number):
        """Place specified number of grains on space randomly.

        Args:
            number (int): Number of grains to put on space.
        """
        if number > self.size[0] * self.size[1]:
            raise Exception("[ERROR] Number of grains higher than space's"
                            " cells.")

        coords = random.sample(self.__get_cells_coords(), number)
        grain_no = 1
        for coord in coords:
            self.cells[coord[0]][coord[1]].value = grain_no
            self.cells[coord[0]][coord[1]].color = self.colors[grain_no-1]
            grain_no += 1

    def __str__(self):
        space_str = ''
        for row in self.cells:
            for col in row:
                space_str += str(col) + ' '
            space_str += '\n'

        return space_str


class Space3D:
    pass
