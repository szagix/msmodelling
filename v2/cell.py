from config import Config

class Cell:
    def __init__(self, x, y, **kwargs):
        self.x = x
        self.y = y
        self.grain = kwargs.get('grain', None)
        self.color = kwargs.get('color', (255, 255, 255))
        self.prec = kwargs.get('prec', False)
        # self.boundary = kwargs.get('boundary', False)

    def free_moore(self):
        """Get free Moore neighborhood cell's coordinates."""
        conf = Config()
        neighbors = []

        if self.x - 1 >= 0:
            neighbors.append((self.x - 1, self.y))
            if self.y - 1 >= 0:
                neighbors.append((self.x - 1, self.y - 1))
            if self.y + 1 < conf.height:
                neighbors.append((self.x - 1, self.y + 1))
        if self.x + 1 < conf.width:
            neighbors.append((self.x + 1, self.y))
            if self.y - 1 >= 0:
                neighbors.append((self.x + 1, self.y - 1))
            if self.y + 1 < conf.height:
                neighbors.append((self.x + 1, self.y + 1))
        if self.y - 1 >= 0:
            neighbors.append((self.x, self.y - 1))
        if self.y + 1 < conf.height:
            neighbors.append((self.x, self.y + 1))

        return _get_free(neighbors)

    def free_neumann(self):
        """Get free Von Neumann neighborhood cell's coordinates."""
        conf = Config()
        neighbors = []

        if self.x - 1 >= 0:
            neighbors.append((self.x - 1, self.y))
        if self.x + 1 < conf.width:
            neighbors.append((self.x + 1, self.y))
        if self.y - 1 >= 0:
            neighbors.append((self.x, self.y - 1))
        if self.y + 1 < conf.height:
            neighbors.append((self.x, self.y + 1))

        return _get_free(neighbors)

    def grow(self, neighbors):
        conf = Config()
        cells = []

        for x, y in neighbors:
            conf.space.cells[x][y].grain = self.grain
            conf.space.cells[x][y].color = self.color
            conf.space.cells[x][y].prec = self.prec
            cells.append(conf.space.cells[x][y])

        return cells

    def occupied(self):
        if not self.grain and not self.prec:
            return False
        return True

    def reset(self):
        self.grain = None
        self.color = (255, 255, 255)
        self.prec = False

    def to_prec(self):
        self.prec = True
        self.grain = None
        self.color = (1, 1, 1)
        # self.boundary = False


def _get_free(neighbors):
    conf = Config()
    free = []

    for x, y in neighbors:
        c = conf.space.cells[x][y]
        if c.grain == None and not c.prec:
            free.append((x, y))

    return free
