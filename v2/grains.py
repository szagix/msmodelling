from config import Config

class Grain:
    def __init__(self, id, color):
        self.id = id
        self.color = color
        self.boundaries = []
        self.active = False

    def __str__(self):
        return str(self.id)

    def grow(self):
        conf = Config()
        new_boundaries = []

        for cell in self.boundaries:
            if conf.neighborhood == 'Moore':
                new_boundaries.extend(cell.grow(cell.free_moore()))
            elif conf.neighborhood == 'Von Neumann':
                new_boundaries.extend(cell.grow(cell.free_neumann()))

        self.boundaries = new_boundaries

        return self.boundaries
