from config import Config

class Prec:
    def __init__(self, x, y, color=(1, 1, 1)):
        self.x = x
        self.y = y
        self.color = color

        conf = Config()
        conf.space.at(x, y).to_prec()

    def grow(self, radius):
        conf = Config()

        x_beg = self.x - radius
        y_beg = self.y - radius
        x_end = self.x + radius
        y_end = self.y + radius

        if x_beg < 0:
            x_beg = 0
        if y_beg < 0:
            y_beg = 0

        if x_end >= conf.width:
            x_end = conf.width - 1
        if y_end >= conf.height:
            y_end = conf.height - 1

        new_coords = []
        for x in range(x_beg, x_end + 1):
            for y in range(y_beg, y_end + 1):
                if (x-self.x)*(x-self.x) + (y-self.y)*(y-self.y) <= radius*radius:
                    conf.space.at(x, y).to_prec()
                    new_coords.append(conf.space.at(x, y))

        return new_coords
