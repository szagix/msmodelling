import sys
from img import draw_space, draw_points, remove_colors, restore_colors
from space import Space
from config import Config
from grains import Grain
from tkinter import filedialog
from PIL import Image

def choose_color():
    conf = Config()

    # Check for selected grains
    if not conf.active_grains:
        print('[WARNING] No grains selected.')
        return

def constraints():
    conf = Config()

    # Check if all required values are set
    req_vals = not conf.width or not conf.height or not conf.grains \
               or conf.neighborhood == 'Choose...'
    if req_vals:
        print('[WARNING] Not all required values set.')
        return False

    # Check if width and height are within the limits
    width_limit = 1024
    height_limit = 1024
    if int(conf.width) > width_limit or int(conf.height) > height_limit:
        print('[WARNING] Too big width or height.')
        print('[WARNING] Max. width: {}\nMax. height: {}'.format(width_limit,
                                                                 height_limit))
        return False

    # Disallow growing precipitates on grain boundaries on the beginning of
    # grain growth
    if conf.pp_time == 'Beginning' and conf.pp_pos == 'Random grain boundaries':
        print('[WARNING] Growing precipitates on grain boundaries when '
              'grains are still growing is disallowed.')
        return False

    return True

def export():
    conf = Config()
    filename = filedialog.asksaveasfilename(
        initialdir=".",
        title="Export to file",
        filetypes=(("CSV, Comma-separated Value","*.csv"),
                   ("BMP, Bitmap", "*.bmp"),))

    if not filename:
        return

    if filename[-4:] == '.csv':
        try:
            with open(filename, mode='w') as new_file:
                for x in range(conf.width):
                    for y in range(conf.height):
                        c = conf.space.at(x, y)
                        new_file.write('{};{};{};{};{}\n'.format(c.x, c.y,
                                                                 c.grain,
                                                                 c.color,
                                                                 c.prec))
        except IOError:
            print('[ERROR] Unable to save file.', file=sys.stderr)
        else:
            print('[SUCCESS] File saved: {}'.format(filename))
    elif filename[-4:] == '.bmp':
        try:
            conf.image.save(filename, format='bmp')
        except ValueError:
            print('[ERROR] Unknown format specified. File not saved.',
                  file=sys.stderr)
        except IOError:
            print('[ERROR] Unable to save image file.', file=sys.stderr)
        else:
            print('[SUCCESS] File saved: {}'.format(filename))

def grow():
    conf = Config()

    if conf.space is None:
        conf.space = Space(conf.width, conf.height)
        conf.space.init_cells()
        conf.space.init_grains()
    else:
        conf.space.append_grains()

    if conf.pp_time == 'Beginning':
        conf.space.init_precipitates()

    draw_space()

    i = 0
    while not conf.space.occupied() and i < conf.MAX_ITER:
        for grain in conf.space.grains:
            if not grain.active:
                draw_points(grain.grow())
        if conf.pp_time == "Beginning" and i < conf.pp_rad:
            for prec in conf.space.precs:
                draw_points(prec.grow(i))
        i += 1

    if conf.pp_time == 'End':
        conf.space.init_precipitates()

        for j in range(conf.pp_rad):
            for prec in conf.space.precs:
                draw_points(prec.grow(j))

    print('[DEBUG] Grains drawn. Iterations: {}'.format(i))
    conf.drawn = True

    conf.app.canvas.bind('<ButtonPress-1>', toggle_grain)

def import_file():
    conf = Config()
    filename = filedialog.askopenfilename(
        initialdir=".",
        title="Import from file",
        filetypes=(("CSV, Comma-separated Value", "*.csv"),
                   ("BMP, Bitmap", "*.bmp")))

    if not filename:
        return

    if filename[-4:] == '.csv':
        with open(filename, mode='r') as imported:
            max_x = 0
            max_y = 0
            for line in imported.readlines():
                data = line.split(';')
                if int(data[0]) > max_x:
                    max_x = int(data[0])
                if int(data[1]) > max_y:
                    max_y = int(data[1])
            conf.width = max_x + 1
            conf.height = max_y + 1
            conf.space = Space(max_x, max_y)
            conf.space.init_cells()
            imported.seek(0)
            for line in imported.readlines():
                data = line.split(';')
                conf.space.at(int(data[0]), int(data[1])).grain = data[2]
                color = data[3].replace(' ', '').replace('(', '').replace(')', '').split(',')
                conf.space.at(int(data[0]), int(data[1])).color = (int(color[0]), int(color[1]), int(color[2]))
                conf.space.at(int(data[0]), int(data[1])).prec = data[4]
        draw_space()
    elif filename[-4:] == '.bmp':
        with Image.open(filename) as imported:
            conf.width = imported.width
            conf.height = imported.height
            conf.space = Space(conf.width, conf.height)
            conf.space.init_cells()
            conf.space.grains = []
            conf.image = imported
            colors = [elem[1] for elem in imported.getcolors()]
            grains_num = 0
            for i, c in enumerate(colors):
                if c != (0, 0, 0) and c != (255, 255, 255) and c != (1, 1, 1):
                    conf.space.grains.append(Grain(i, c))
                    grains_num += 1
            conf.grains = grains_num
            for x in range(conf.width):
                for y in range(conf.height):
                    conf.space.at(x, y).color = conf.image.getpixel((x, y))
                    conf.space.at(x, y).grain = conf.space.get_grain_by_color(conf.space.at(x, y).color)
            draw_points([])

def mark_gb():
    conf = Config()

    # Check if image is drawn and capable of marking grain boundaries
    if not conf.drawn:
        print('[WARNING] Image not drawn yet.')
        return

    color = None
    if not conf.gb_marked:
        color = (0, 0, 0)
    draw_points(conf.space.grain_boundaries(), color)

    conf.gb_marked = not conf.gb_marked

def remove_inactive():
    conf = Config()

    for x in range(conf.width):
        for y in range(conf.height):
            if conf.space.cells[x][y].grain not in conf.active_grains:
                conf.space.cells[x][y].reset()
                draw_points([conf.space.cells[x][y]], update=False)
    conf.space.grains = conf.active_grains

    draw_points([])

def reset():
    conf = Config()
    app_ref = conf.app

    conf.app.canvas.delete('all')

    conf.app.option['neighborhood'].set('Choose...')
    conf.app.option['pp_pos'].set('Choose...')
    conf.app.option['pp_time'].set('Choose...')

    conf.space = None

    del conf.width
    del conf.height
    del conf.grains
    del conf.gb_size
    del conf.pp_num
    del conf.pp_rad
    del conf

    Config(app_ref)

def toggle_colors():
    conf = Config()

    if conf.colorized:
        remove_colors((0, 0, 0))
    else:
        restore_colors((0, 0, 0))

    conf.colorized = not conf.colorized

def toggle_grain(event):
    conf = Config()
    x = event.x
    y = event.y
    print('[DEBUG] Clicked ({}, {})'.format(x, y))

    if x >= conf.width or y >= conf.height:
        return

    grain = conf.space.at(x, y).grain
    if grain not in conf.active_grains and grain is not None:
        conf.active_grains.append(grain)
        grain.active = True
    else:
        conf.active_grains.remove(grain)
        grain.active = False

    print('[DEBUG] Active grains: {}'.format(conf.active_grains))
