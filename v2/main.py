from gui import Gui
from config import Config

if __name__ == "__main__":
    app = Gui()
    Config(app)
    app.start()
