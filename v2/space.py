from cell import Cell
from prec import Prec
from grains import Grain
from config import Config
from colors import get_random_color
from random import randrange, seed

class Space:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
        self.cells = None
        self.grains = None
        self.precs = None

    def append_grains(self):
        seed()
        conf = Config()
        new_grains = []
        new_grains.extend(self.grains)
        ids = [g.id for g in new_grains]

        for i in range(1, conf.grains + 1):
            id = i
            while id in ids:
                id += 1
            g = Grain(id, get_random_color())
            new_grains.append(g)

        self.grains = new_grains

        for g in self.grains:
            x = randrange(conf.width)
            y = randrange(conf.height)
            while self.cells[x][y].occupied():
                x = randrange(conf.width)
                y = randrange(conf.height)
            g.boundaries = [self.cells[x][y]]

            self.cells[x][y].grain = g
            self.cells[x][y].color = g.color

    def at(self, x, y):
        return self.cells[x][y]

    def get_grain(self, id):
        for g in self.grains:
            if g.id == id:
                return g

    def get_grain_by_color(self, c):
        for g in self.grains:
            if g.color == c:
                return g

    def grain_boundaries(self):
        """Returns list of coordinates of grain boundaries."""
        coords = []

        for x in range(self.x):
            for y in range(self.y):
                if self.cells[x][y].grain:
                    val = self.cells[x][y].grain.id
                    is_boundary = False
                    if y - 1 >= 0:
                        if self.cells[x][y-1].grain:
                            if self.cells[x][y-1].grain.id != val:
                                is_boundary = True
                    if x - 1 >= 0:
                        if self.cells[x-1][y].grain:
                            if self.cells[x-1][y].grain.id != val:
                                is_boundary = True
                    if y + 1 < self.y:
                        if self.cells[x][y+1].grain:
                            if self.cells[x][y+1].grain.id != val:
                                is_boundary = True
                    if x + 1 < self.x:
                        if self.cells[x+1][y].grain:
                            if self.cells[x+1][y].grain.id != val:
                                is_boundary = True
                    if is_boundary:
                        coords.append(self.cells[x][y])

        return coords

    def init_cells(self):
        conf = Config()
        self.cells = [[Cell(x, y) for y in range(conf.height)] \
                      for x in range(conf.width)]

    def init_grains(self):
        seed()
        conf = Config()
        self.grains = [Grain(id, get_random_color()) for id in \
                       range(1, conf.grains+1)]

        for g in self.grains:
            x = randrange(conf.width)
            y = randrange(conf.height)
            while self.cells[x][y].occupied():
                x = randrange(conf.width)
                y = randrange(conf.height)
            g.boundaries = [self.cells[x][y]]

            self.cells[x][y].grain = g
            self.cells[x][y].color = g.color

    def init_precipitates(self):
        seed()
        conf = Config()

        self.precs = []
        if conf.pp_pos == 'Random':
            for i in range(conf.pp_num):
                x = randrange(conf.width)
                y = randrange(conf.height)
                self.precs.append(Prec(x, y))
                conf.space.at(x, y).to_prec()
        elif conf.pp_pos == 'Random grain boundaries':
            gb = self.grain_boundaries()

    def occupied(self):
        for x in range(self.x):
            for y in range(self.y):
                if not self.cells[x][y].occupied():
                    return False
        return True
