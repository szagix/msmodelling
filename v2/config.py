class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args,
                                                                 **kwargs)
        return cls._instances[cls]

class Config(metaclass=Singleton):
    def __init__(self, app=None):
        self.MAX_ITER = 5000
        self.app = app

        self.gb_marked = False
        self.drawn = False
        self.colorized = True

        self.space = None
        self.active_grains = []

        self.image = None
        self.img_tk = None
        self.draw = None

    def __str__(self):
        return self.app

    @property
    def gb_size(self):
        return int(self.app.entry['gb_size'].get())

    @gb_size.setter
    def gb_size(self, val):
        self.app.entry['gb_size'].delete(0, 'end')
        self.app.entry['gb_size'].insert(0, str(val))

    @gb_size.deleter
    def gb_size(self):
        self.app.entry['gb_size'].delete(0, 'end')

    @property
    def grains(self):
        return int(self.app.entry['grains'].get())

    @grains.setter
    def grains(self, val):
        self.app.entry['grains'].delete(0, 'end')
        self.app.entry['grains'].insert(0, str(val))

    @grains.deleter
    def grains(self):
        self.app.entry['grains'].delete(0, 'end')

    @property
    def height(self):
        return int(self.app.entry['height'].get())

    @height.setter
    def height(self, val):
        self.app.entry['height'].delete(0, 'end')
        self.app.entry['height'].insert(0, str(val))

    @height.deleter
    def height(self):
        self.app.entry['height'].delete(0, 'end')

    @property
    def neighborhood(self):
        return self.app.option['neighborhood'].get()

    @property
    def pp_num(self):
        return int(self.app.entry['pp_num'].get())

    @pp_num.setter
    def pp_num(self, val):
        self.app.entry['pp_num'].delete(0, 'end')
        self.app.entry['pp_num'].insert(0, str(val))

    @pp_num.deleter
    def pp_num(self):
        self.app.entry['pp_num'].delete(0, 'end')

    @property
    def pp_pos(self):
        return self.app.option['pp_pos'].get()

    @property
    def pp_rad(self):
        return int(self.app.entry['pp_rad'].get())

    @pp_rad.setter
    def pp_rad(self, val):
        self.app.entry['pp_rad'].delete(0, 'end')
        self.app.entry['pp_rad'].insert(0, str(val))

    @pp_rad.deleter
    def pp_rad(self):
        self.app.entry['pp_rad'].delete(0, 'end')

    @property
    def pp_time(self):
        return self.app.option['pp_time'].get()

    @property
    def width(self):
        return int(self.app.entry['width'].get())

    @width.setter
    def width(self, val):
        self.app.entry['width'].delete(0, 'end')
        self.app.entry['width'].insert(0, str(val))

    @width.deleter
    def width(self):
        self.app.entry['width'].delete(0, 'end')
