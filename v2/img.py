from space import Space
from config import Config
from PIL import Image, ImageTk, ImageDraw

def compare_px(px1, px2):
    return px1[0] == px2[0] and px1[1] == px2[1] and px1[2] == px2[2]

def draw_points(coords, custom_color=None, update=True):
    conf = Config()

    if custom_color:
        for c in coords:
            conf.draw.point([c.x, c.y], fill=custom_color)
    else:
        for c in coords:
            conf.draw.point([c.x, c.y], fill=c.color)

    if update:
        conf.img_tk = ImageTk.PhotoImage(conf.image)
        conf.app.canvas.create_image(0, 0, image=conf.img_tk, anchor='nw')
        conf.app.window.update()

def draw_space():
    conf = Config()

    conf.image = Image.new('RGB', (conf.width, conf.height), (255, 255, 255))
    conf.draw = ImageDraw.Draw(conf.image)

    for x in range(conf.width):
        for y in range(conf.height):
            conf.draw.point([x, y], fill=conf.space.cells[x][y].color)

    conf.img_tk = ImageTk.PhotoImage(conf.image)
    conf.app.canvas.create_image(0, 0, image=conf.img_tk, anchor='nw')
    conf.app.window.update()

def remove_colors(keep=None):
    conf = Config()

    for x in range(conf.width):
        for y in range(conf.height):
            if not compare_px(conf.image.getpixel((x, y)), keep):
                conf.draw.point([x, y], fill=(255, 255, 255))

    conf.img_tk = ImageTk.PhotoImage(conf.image)
    conf.app.canvas.create_image(0, 0, image=conf.img_tk, anchor='nw')
    conf.app.window.update()

def restore_colors(keep=None):
    conf = Config()

    for x in range(conf.width):
        for y in range(conf.height):
            if not compare_px(conf.image.getpixel((x, y)), keep):
                conf.draw.point([x, y], fill=conf.space.cells[x][y].color)

    conf.img_tk = ImageTk.PhotoImage(conf.image)
    conf.app.canvas.create_image(0, 0, image=conf.img_tk, anchor='nw')
    conf.app.window.update()
