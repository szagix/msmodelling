import actions
import tkinter as tk
# from tkinter import messagebox, filedialog

SGRID = [[(x, y) for y in range(15, 900, 35)] for x in range(15, 800, 100)]
MGRID = [[(x, y) for y in range(15, 900, 35)] for x in range(15, 800, 200)]
LGRID = [[(x, y) for y in range(15, 900, 35)] for x in range(15, 800, 400)]

class Gui:
    def __init__(self):
        self.window = tk.Tk()
        self.label = dict()
        self.entry = dict()
        self.option = dict()
        self.button = dict()
        self.canvas = None

        self.window.title("Grain Growth Simulator")
        self.window.geometry('830x900')

        # Labels
        self.add_label('width', 'Space width', MGRID[0][0])
        self.add_label('height', 'Space height', MGRID[0][1])
        self.add_label('grains', 'Number of grains', MGRID[0][2])
        self.add_label('neighborhood', 'Neighborhood', MGRID[0][3])
        self.add_label('gb_size', 'Grain Boundary size', MGRID[0][4])
        self.add_label('pp_time', 'Precipitate growth time', MGRID[2][0])
        self.add_label('pp_pos', 'Precipitate position', MGRID[2][1])
        self.add_label('pp_num', 'Precipitates number', MGRID[2][2])
        self.add_label('pp_rad', 'Precipitate radius', MGRID[2][3])

        # Text Fields
        self.add_text_field('width', 15, MGRID[1][0])
        self.add_text_field('height', 15, MGRID[1][1])
        self.add_text_field('grains', 15, MGRID[1][2])
        self.add_text_field('gb_size', 15, MGRID[1][4])
        self.add_text_field('pp_num', 15, MGRID[3][2])
        self.add_text_field('pp_rad', 15, MGRID[3][3])

        # Option Menus
        self.add_option_menu('neighborhood', 'Choose...', MGRID[1][3],
                             'Moore', 'Von Neumann')
        self.add_option_menu('pp_time', 'Choose...', MGRID[3][0],
                             'End', 'Beginning')
        self.add_option_menu('pp_pos', 'Choose...', MGRID[3][1],
                             'Random', 'Random grain boundaries')

        # Buttons
        self.add_button('grow', 'Grow', actions.grow, SGRID[0][5])
        self.add_button('mark_gb', 'Mark GB', actions.mark_gb, SGRID[1][5])
        self.add_button('toggle_colors', 'Toggle colors', actions.toggle_colors,
                        SGRID[2][5])
        self.add_button('color', 'Color', actions.choose_color, SGRID[3][5])
        self.add_button('del_inactive', 'Del Inactive', actions.remove_inactive,
                        SGRID[4][5])
        self.add_button('check', 'Check', actions.constraints, SGRID[0][6])
        self.add_button('export', 'Export', actions.export, SGRID[1][6])
        self.add_button('import', 'Import', actions.import_file, SGRID[2][6])
        self.add_button('reset', 'Reset', actions.reset, SGRID[3][6])

        # Canvas
        self.add_canvas(LGRID[0][7])

        # Events
        self.canvas.bind('<ButtonPress-1>', actions.toggle_grain)

    def add_button(self, name, text, command, xy, width=8):
        self.button[name] = tk.Button(self.window, text=text, command=command,
                                      width=width)
        self.button[name].place(x=xy[0], y=xy[1])

    def add_canvas(self, xy):
        self.canvas = tk.Canvas(self.window, width=800, height=600)
        self.canvas.pack(side='bottom', fill='both', expand='yes')
        self.canvas.place(x=xy[0], y=xy[1])

    def add_label(self, name, text, xy):
        self.label[name] = tk.Label(self.window, text=text)
        self.label[name].place(x=xy[0], y=xy[1])

    def add_option_menu(self, name, text, xy, option, *args):
        self.option[name] = tk.StringVar(self.window)
        self.option[name].set(text)
        tk.OptionMenu(self.window, self.option[name], option, *args).place(
            x=xy[0], y=xy[1])

    def add_text_field(self, name, width, xy):
        self.entry[name] = tk.Entry(self.window, width=width)
        self.entry[name].place(x=xy[0], y=xy[1])

    def start(self):
        self.window.mainloop()
